/*
 *  The ManaPlus Client
 *  Copyright (C) 2014  The ManaPlus Developers
 *
 *  This file is part of The ManaPlus Client.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "lintmanager.h"

#include "localconsts.h"

static void showHelp()
{
    printf("mplint - ManaPlus source code lint\n");
    printf("Usage:\n");
    printf("mplint dir\n");
}

int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        showHelp();
        return 1;
    }
    lint.run(argv[1]);
    return 0;
}
