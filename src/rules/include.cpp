/*
 *  The ManaPlus Client
 *  Copyright (C) 2014  The ManaPlus Developers
 *
 *  This file is part of The ManaPlus Client.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "template.hpp"

registerRuleExt(include, "008", "(.+)[.](cpp|h)")

startRule(include)
{
}

endRule(include)
{
}

parseLineRule(include)
{
    std::string data0 = data;
    trim(data);
    if (data == "#include \"config.h\"")
        return;

    if (findCutFirst(data, "#include \""))
    {
        if (findCutLast(data, "\""))
        {
            if (!fileExists(rootDir + data))
            {
                print("Wrong include " + data0 + ". "
                    "Probably you should use path from src dir,"
                    " or use #include <file.ext>");
            }
        }
    }
    else if (findCutFirst(data, "#include <"))
    {
        if (findCutLast(data, ">"))
        {
            if (fileExists(rootDir + data))
            {
                print("Wrong include " + data0 + ". "
                    "Found local file with same name.");
            }
        }
    }
}

