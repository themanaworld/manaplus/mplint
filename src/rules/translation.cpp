/*
 *  The ManaPlus Client
 *  Copyright (C) 2014  The ManaPlus Developers
 *
 *  This file is part of The ManaPlus Client.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "template.hpp"

registerRuleExt(Translation, "014", "(.+)[.](cpp|h|hpp|cc|inc)")

startRule(Translation)
{
    if (strEndWith(file, "/catch.hpp") ||
        strEndWith(file, "/localconsts.h") ||
        strEndWith(file, "/socialtabbase.h") ||
        strEndWith(file, "/utils/gettext.h"))
    {
        terminateRule();
    }
}

endRule(Translation)
{
}

bool gotComment(false);

parseLineRule(Translation)
{
    trim(data);
    if (data.find("__attribute__") != std::string::npos ||
        data.find(",J?4P'.P\"_(\\?d'.,") != std::string::npos)
    {
        // nothing
    }
    else if (data.find("_(") != std::string::npos ||
             data.find("N_(") != std::string::npos)
    {
        if (!gotComment)
            print("Missing \"// TRANSLATORS: \" comment before this line.");
        else
            gotComment = false;
    }
    else if (data.find("// TRANSLATORS: ") == 0)
    {
        gotComment = true;
    }
    else if (strStartWith(data, "//") && gotComment)
    {
        // nothing
    }
    else
    {
        gotComment = false;
    }
}
