/*
 *  The ManaPlus Client
 *  Copyright (C) 2014  The ManaPlus Developers
 *
 *  This file is part of The ManaPlus Client.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "template.hpp"

registerRuleExt(debugH, "003", "(.+)[.]h")

startRule(debugH)
{
}

endRule(debugH)
{
}

parseLineRule(debugH)
{
    trim(data);
    if (data == "#include \"debug.h\"")
    {
        print("Dont use #include \"debug.h\" in .h files. "
            "Probably need replace it to #include \"localconsts.h\"");
    }
}


bool foundDebug(false);

registerRuleExt(debugCpp, "004", "(.+)[.]cpp")

startRule(debugCpp)
{
    if (isMatch(file, "(.*)[/]debug[/]([^/]*)[.](cpp|h)"))
        terminateRule();
    foundDebug = false;
}

endRule(debugCpp)
{
    if (!foundDebug)
    {
        print("Missing #include \"debug.h\". "
            "It need for profiling and memory debugging.");
    }
}

parseLineRule(debugCpp)
{
    // need check what debug.h will be last include in file
    trim(data);
    if (foundDebug)
    {
        if (!data.empty() && data[0] != '#')
        {
            terminateRule();
        }
        else
        {
            if (strStartWith(data, "#include ")
                && data != "#include \"debug.h\"")
            {
                if (!strEndWith(file, "/utils/copynpaste.cpp"))
                    print("Last include must be #include \"debug.h\"");
                terminateRule();
            }
        }
    }
    else
    {
        if (data == "#include \"debug.h\"")
            foundDebug = true;
    }
}
