/*
 *  The ManaPlus Client
 *  Copyright (C) 2014  The ManaPlus Developers
 *
 *  This file is part of The ManaPlus Client.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

void func1() {
    if (a == 10)
    {
        if (b == 20)
        {
        }
    }
}

void func2()
{
    if (a == 10) {
        if (b == 20)
        {
        }
    }
}

void func3()
{
    if (a == 10)
    {
        if (b == 20) {
        }
    }
}

void func4()
{
    if (a == 10)
    {
        if (b == 20)
        {
        } else
        {
        }
    }
}

void func5()
{
    if (a == 10)
    {
        if (b == 20)
        {
        }
        else {
        }
    }
}

void func6()
{
    if (a == 10) {
        if (b == 20) {
        } else {
        }
    }
}

void func7()
{
    type *var = new type();
    type *var2;
    type2 *var3 = new type2(123);
}
