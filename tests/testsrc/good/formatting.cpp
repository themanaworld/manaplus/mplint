/*
 *  The ManaPlus Client
 *  Copyright (C) 2014  The ManaPlus Developers
 *
 *  This file is part of The ManaPlus Client.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "lintmanager.h"

#include "debug.h"

class Test1 final
{
    public:
        A_DELETE_COPY(Test1);
        Test1::Test1()
        {
        }
}

struct Test2 final
{
    protected:
        A_DELETE_COPY(Test2);
        Test2::Test2() :
            data1(),
            data2()
        {
        }
}

struct Test3 final
{
    A_DEFAULT_COPY(Test3);
    private:
        Test3::Test3() :
            data1(),
            data2()
        {
        }
}

struct ParticleTimer final
{
    ParticleTimer(Particle *const particle0,
                  const int endTime0) A_NONNULL(2) :
        particle(particle0),
        endTime(endTime0)
    {
    }

    A_DELETE_COPY(ParticleTimer);

    Particle *particle;
    int endTime;
    Particle *const particle;
    const int endTime;
};
